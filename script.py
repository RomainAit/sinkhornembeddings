from gensim.models.word2vec import Text8Corpus
from gensim.corpora import Dictionary

import torch
import torch.nn as NN
import torch.nn.functional as functional
import torch.optim as opt

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
import itertools
import sys

M = int(sys.argv[1])
m = 1
l = 0.05

texte = Text8Corpus("text8", max_sentence_length=200)
phrases = list(itertools.islice(texte, None))
phrases = list(itertools.islice(texte, 0, 20000,None))

dictio = Dictionary(phrases)
dictio.filter_extremes(no_below=1, no_above=0.05, keep_n=800)

phrasesId = [[i for i in item if i != -1] for item in [dictio.doc2idx(p) for p in phrases]]

context = []

for p in phrasesId:
    contextPhrase = []
    for i in range(len(p)):
        contextMot = []
        for j in range(max(i-2,0), min(len(p),i+3)):
            contextMot.append(p[j])
        contextPhrase.append((p[i], contextMot))
    context.append(contextPhrase)




def sinkhorn(x, y, l, M):
    
    D = torch.sum((torch.abs(x.unsqueeze(1) - y.unsqueeze(0))), 2)
    K = torch.exp(-D/l)
    
    c = 1/M*torch.ones(M)
    u = 1/M*torch.ones(M)
    v = 1/M*torch.ones(M)
    
    for i in range(0,30):
        
        r = u / torch.mv(K, c)
        c = v / torch.mv(K.t(), r)
    
    
    T = torch.mm(torch.mm(torch.diag(r), K), torch.diag(c))
    
    return torch.trace(torch.mm(D.t(), T))


def toVect(pos):
    Position = torch.zeros(800)
    Position[pos] = 1
    return Position


class Reseau(NN.Module):
    
    def __init__(self):
    
        super(Reseau, self).__init__()
        self.fc1 = NN.Linear(800, 64)
        self.fc2 = NN.Linear(64, 2*M)

   
    def forward(self, x):
        
        x = self.fc1(x)
        x = self.fc2(x)
        
        return x


    



reseau = Reseau()
opti = opt.Adam(reseau.parameters())

noPhrase = 0
while noPhrase < len(phrasesId) :
    while len(context[noPhrase]) < 15 or len(list(set(phrasesId[noPhrase]))) < 7: 
        ##Permet d'éviter les bugs quand un mot se repète trop de fois, ou les phrases trop courtes.
        noPhrase = noPhrase+1

    LOSS = torch.zeros(1)
    opti.zero_grad()
    
    dataPhrase = []
    

    for (mot, ctx) in context[noPhrase]:
        notIn = []
        for it in range(0,len(ctx)):
            motNotIn = random.choice(phrasesId[noPhrase])
            while motNotIn in ctx:
                motNotIn = random.choice(phrasesId[noPhrase]) 
                ## Sélectionne aléatoirement un mot hors contexte pour chaque mot dans le contexte.
                ## Ceci est toujours possible grace aux conditions imposées sur les phrases d'entrainement précedemment.
                    
            notIn.append(motNotIn)
            dataPhrase.append((mot, ctx, notIn))
    

    for (mot, cont, notIn) in dataPhrase:
        ## Entraine le réseau, pour chaque mot de chaque phrase
        
        
        for motNotIn in notIn:
            LOSS = functional.relu(m-sinkhorn(reseau(toVect(mot)).view((M,2)),reseau(toVect(motNotIn)).view((M,2)),l,M))**2 + LOSS
        
        for c in cont:
            LOSS = sinkhorn(reseau(toVect(mot)).view((M,2)),reseau(toVect(c)).view((M,2)),l,M)**2 + LOSS
        
    LOSS.backward()
    opti.step()
    
    if noPhrase%100 == 0 or noPhrase == len(phrasesId)-1:
        torch.save(reseau, 'reseau.pth') ## Enregistre le réseau régulièrement.
        
    noPhrase = noPhrase+1
